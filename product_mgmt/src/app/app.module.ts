import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent }  from './app.component';
import { DirectiveComponent }  from './directive.component';
import { MyDirComponent } from './mydir.component';

import { FormsModule } from '@angular/forms';

//import {Routes} from '@angular/router';
//import { Router }  from '@angular/router'; 
import { AppProduct } from './app.product';
import { AppInventory } from './app.inventory';
import { LoginComponent } from './login/login.component';

/*@Routes([
  {path: '/',     		 component: LoginComponent },
  {path: '/product',     component: AppProduct },
  {path: '/inventory',   component: AppInventory }
])*/


@NgModule({
  imports:      [ BrowserModule, FormsModule],
  declarations: [ AppComponent, DirectiveComponent, MyDirComponent, AppProduct, AppInventory, LoginComponent],
  bootstrap:    [ LoginComponent ]
})
export class AppModule { }


/* Optional Parameters demo 
function dispUserDetails(id: number, name: string, mailId?: string) { 
   console.log("ID:", id); 
   console.log("Name",name); 
   
   if(mailId != undefined) {
	 console.log("Email Id",mailId); 
	}  
}
	
dispUserDetails(123,"John");
dispUserDetails(111,"mary","mary@xyz.com");
*/


/* Rest Parameters OR variable arguments */

function addNumbers(...nums:number[]) {  
   var i;   
   var sum:number = 0; 
   
   for(i = 0;i<nums.length;i++) { 
      sum = sum + nums[i]; 
   } 
   console.log("sum of the numbers",sum) 
} 
addNumbers(1,2,3) 
addNumbers(10,10,10,10,10)

/* Default parameters */

function calculate_discount(price:number, rate:number = 0.50) { 
   var discount = price * rate; 
   console.log("Discount Amount: ",discount); 
} 
calculate_discount(1000) 
calculate_discount(1000, 0.30)
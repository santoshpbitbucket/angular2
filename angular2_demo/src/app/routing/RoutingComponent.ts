import { Component} from '@angular/core';
import { Router }  from '@angular/router'; 

@Component({
   selector: 'my-app',
   templateUrl: 'app/routing/routing.component.html'
})


export class RoutingComponent {
	constructor(private _router: Router){} 
}
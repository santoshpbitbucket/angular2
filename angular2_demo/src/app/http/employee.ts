export interface IEmployee {
   employeetID: number;
   employeeName: string;
}
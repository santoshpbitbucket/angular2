import { Component } from '@angular/core';
import { IEmployee } from './employee';
import { EmployeeService } from './employeeservice';
import { Http , Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Component ({
   selector: 'my-app',
   template: '<div>Hello</div>',
   providers: [EmployeeService]
})

export class UseOfHttp {
	
   employees: IEmployee[];
   constructor(private employeeService: EmployeeService) {
   }
   
   ngOnInit() : void {
      this.employeeService.getEmployees()
      .subscribe(employees => this.employees = employees);
   }
}
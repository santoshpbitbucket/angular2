class Greeting { 
    greeting: string;
    constructor(message: string) {
        this.greeting = message;
    }
    greet() : string {
        return "Hello, " + this.greeting;
    }
} 

var obj: Greeting = new Greeting("world"); 
var message: string = obj.greet();
console.log(message)
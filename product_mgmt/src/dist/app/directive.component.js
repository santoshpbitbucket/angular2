"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var DirectiveComponent = (function () {
    function DirectiveComponent() {
        this.appTitle = 'Product Management';
        this.appStatus = true;
        this.employeeList = [{
                "ID": "101",
                "Name": "Santosh",
                "url": 'app/Images/one.jpg'
            },
            {
                "ID": "102",
                "Name": "Ashutosh",
                "url": 'app/Images/two.jpg'
            }];
    }
    return DirectiveComponent;
}());
DirectiveComponent = __decorate([
    core_1.Component({
        selector: 'add-product',
        templateUrl: 'app/directive.component.html'
    })
], DirectiveComponent);
exports.DirectiveComponent = DirectiveComponent;

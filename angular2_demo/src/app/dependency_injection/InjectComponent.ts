import { Component} from '@angular/core';
import { InjectService } from './InjectService'

@Component ( {
	selector: "my-app",
	templateUrl: 'app/dependency_injection/inject.html',
	providers: [InjectService]
})


export class InjectComponent {
	
	message:string = "";
	
	constructor(private injectService: InjectService) { 
		console.log("service injected successfully");
	}
	
	ngOnInit():void {
		this.message = this.injectService.getHelloMessage();
	}
}
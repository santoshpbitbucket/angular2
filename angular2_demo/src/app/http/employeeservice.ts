import { Injectable } from '@angular/core';
import { Http , Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import { IEmployee } from './employee';

@Injectable()
export class EmployeeService {
   private employeeurl='app/http/employees.json';
   
   constructor(private http: Http){}
   
   getEmployees(): Observable<IEmployee[]> {
      return this.http.get(this.employeeurl)
      .map((response: Response) => <IEmployee[]> response.json())
      .do(data => console.log(JSON.stringify(data)));
   }
}
import { Component} from '@angular/core';

import { CustomMuliplierPipe } from './CustomMuliplierPipe';
import { FilterString } from './FilterString';

@Component ( {
	selector: "my-app",
	//template: '<p> {{2 | Multiplier:3}}</p>', 
	templateUrl: 'app/pipes/pipes.html',
	providers: [CustomMuliplierPipe, FilterString],
	pipes:[CustomMuliplierPipe, FilterString]
})


export class PipesComponent {
     appList: string[] = ["Binding", "Display", "Services"]; 
	 
	 sliceText: string = "This is used to slice a piece of data from the input string.";
	 todayDate = new Date();
	 
	 languagesNames = [
				'java',
				'j2EE',
				'net',
				'php',
				'nodejs',
				'javascript',
				'angularjs'
			];

}
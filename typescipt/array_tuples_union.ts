/* Arrays starts */



var alphas:string[]; 
alphas = ["1","2","3","4"] 
console.log(alphas[0]); 
console.log(alphas[1]);

var nums:number[] = [1,2,3,3] 

var arr_names:number[] = new Array(4)  

for(var i = 0; i<arr_names.length; i++) { 
   arr_names[i] = i * 2 
   console.log(arr_names[i]) 
}

var names:string[] = new Array("Mary", "Tom", "Jack", "Jill") 

for(var i = 0;i<names.length;i++) { 
   console.log(names[i]) 
}
/* Arrays ends */


/* tuple starts */

var mytuple = [10, "Hello"];

console.log(mytuple[0]) 
console.log(mytuple[1])

var yourtuple = []; 
yourtuple[0] = "world" 
yourtuple[1] = 234


console.log(yourtuple[0]) 
console.log(yourtuple[1])

// Tuple Operations

var mytuple = [10,"Hello", "World", "typeScript"]; 
console.log("Items before push " + mytuple.length)    // returns the tuple size 

mytuple.push(12)                                    // append value to the tuple 
console.log("Items after push "+ mytuple.length) 
console.log("Items after pop "+ mytuple) 
console.log(mytuple.pop()+" popped from the tuple") // removes and returns the last item
console.log("Items before pop "+ mytuple) 

console.log("Items after pop " + mytuple.length)

//update a tuple element 
mytuple[0] = 121
console.log("Items after update " + mytuple) 


/* tuple ends */

/* union starts */

var val:string|number 
val = 12 
console.log("numeric value of val "+val) 
val = "This is a string" 
console.log("string value of val "+val)


/***************************** Union Type and function parameter ****************************/

function disp(name:string|string[]) { 
   if(typeof name == "string") { 
      console.log(name) 
   } else { 
      var i; 
      
      for(i = 0;i<name.length;i++) { 
         console.log(name[i])
      } 
   } 
} 
disp("mark") 
console.log("Printing names array....") 
disp(["Mark","Tom","Mary","John"])

/***************************** Union Type and Array ****************************/
var arr:number[]|string[]; 
var i:number; 
arr = [1,2,4] 
console.log("**numeric array**")  

for(i = 0;i<arr.length;i++) { 
   console.log(arr[i]) 
}  

arr = ["Mumbai","Pune","Delhi"] 
console.log("**string array**")  

for(i = 0;i<arr.length;i++) { 
   console.log(arr[i]) 
} 

/* union ends */

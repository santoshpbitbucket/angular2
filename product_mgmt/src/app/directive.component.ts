import { Component } from '@angular/core';  

@Component ({
   selector: 'add-product',
   templateUrl: 'app/directive.component.html'
})

export class DirectiveComponent {
   appTitle: string = 'Product Management';
   appStatus: boolean = true;
   employeeList: any[] = [ {
      "ID": "101",
      "Name" : "Santosh",
	  "url": 'app/Images/one.jpg'
   },

   {
      "ID": "102",
      "Name" : "Ashutosh",
	  "url": 'app/Images/two.jpg'
   } ];
}
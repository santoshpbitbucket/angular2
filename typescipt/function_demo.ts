/* simple function starts here */

function hello() {    
   console.log("hello from my side.") 
} 
hello();

/* simple function ends here */

/* Parameterized a Function */

function displayUserInfo(userId:number, userName:string) { 
   console.log(userId) 
   console.log(userName) 
} 
displayUserInfo("Pranav Kumar")


/* Returning a Function */

//function defined 
function greet():string {
   return "Hello World" 
} 

function caller() { 
   var msg = greet();
   console.log(msg) 
} 

//invoke function 
caller()
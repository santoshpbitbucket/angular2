import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent }  from './app.component';
import { ngIfngForDemo }  from './directives/ngIfngForDemo';
import { LoginComponent } from './login/login.component';
import { UseOfHttp } from './http/useofhttp';
import { ExpressionComponent } from './expression/ExpressionComponent'
import { MyDirComponent } from './directives/MyDirComponent'
import { RoutingComponent } from './routing/RoutingComponent'
import { AboutComponent } from './routing/about';
import { HomeComponent } from './routing/home';
import { PageNotFoundComponent } from './routing/PageNotFoundComponent';
import { PipesComponent } from './pipes/PipesComponent';
import { CustomMuliplierPipe } from './pipes/CustomMuliplierPipe';
import { FilterString } from './pipes/FilterString';
import { InjectComponent } from './dependency_injection/InjectComponent';


const appRoutes: Routes([
  {path: '',       component: HomeComponent },
  {path: 'home',   component: HomeComponent },
  {path: 'about',  component: AboutComponent },
  { path: '**',    component: PageNotFoundComponent } 

])

@NgModule({
  imports:      [ BrowserModule, HttpModule, FormsModule, RouterModule, RouterModule.forRoot(appRoutes)],
  declarations: [ AppComponent, LoginComponent, ExpressionComponent, ngIfngForDemo, MyDirComponent, RoutingComponent, HomeComponent, AboutComponent, PageNotFoundComponent, UseOfHttp, PipesComponent, CustomMuliplierPipe, FilterString, InjectComponent],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }


/*
class Shape { 
   Area:number 
   
   constructor(a:number) { 
      this.Area = a 
   } 
} 

class Circle extends Shape { 
   disp():void { 
      console.log("Area of the circle:  "+this.Area) 
   } 
}
  
var obj = new Circle(223); 
obj.disp()
*/

/******************************* Class inheritance and Method Overriding *************************

class PrinterClass { 
   doPrint():void {
      console.log("doPrint() from Parent called…") 
   } 
} 

class StringPrinter extends PrinterClass { 
   doPrint():void { 
      //super.doPrint() 
      console.log("doPrint() is printing a string…")
   } 
} 

var obj = new StringPrinter() 
obj.doPrint()
********/

/******************************* Classes and Interfaces *********************************/

interface ILoan { 
   interest:number;
   display():void;
} 

class AgriLoan implements ILoan { 
   interest:number 
   rebate:number 
   
   constructor(interest:number,rebate:number) { 
      this.interest = interest 
      this.rebate = rebate 
   }
   
   display():void { 
      console.log("Interest is : "+obj.interest+" Rebate is : "+obj.rebate )
   } 
} 

var obj = new AgriLoan(10,1)
obj.display();

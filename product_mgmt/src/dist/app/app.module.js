"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
var app_component_1 = require("./app.component");
var directive_component_1 = require("./directive.component");
var mydir_component_1 = require("./mydir.component");
var forms_1 = require("@angular/forms");
//import {Routes} from '@angular/router';
//import { Router }  from '@angular/router'; 
var app_product_1 = require("./app.product");
var app_inventory_1 = require("./app.inventory");
var login_component_1 = require("./login/login.component");
/*@Routes([
  {path: '/',     		 component: LoginComponent },
  {path: '/product',     component: AppProduct },
  {path: '/inventory',   component: AppInventory }
])*/
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    core_1.NgModule({
        imports: [platform_browser_1.BrowserModule, forms_1.FormsModule],
        declarations: [app_component_1.AppComponent, directive_component_1.DirectiveComponent, mydir_component_1.MyDirComponent, app_product_1.AppProduct, app_inventory_1.AppInventory, login_component_1.LoginComponent],
        bootstrap: [login_component_1.LoginComponent]
    })
], AppModule);
exports.AppModule = AppModule;

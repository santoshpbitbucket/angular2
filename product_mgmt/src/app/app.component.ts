import { Component} from '@angular/core';

@Component({
   selector: 'my-app',
   templateUrl: '/app/app.component.html'
   //styles: [require('./stylesheet/productmgmt.css')],
})

export class AppComponent {

	name:string = "Customer";

	//constructor(private _router: Router){} 

	onBack(): void { 
     // this._router.navigate(['/product']); 
    }
   
    messageText = '';  
    
	onClickMe(yourName:string) {  
      this.messageText = "Hi Reader! " + yourName;  
    }  



}
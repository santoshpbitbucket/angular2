

var myName:string = "John"; 
var marksInMaths:number = 50;
var marksInScience:number = 42.50

var sum = marksInMaths + marksInScience

console.log("myName: " + myName) 
console.log("marks In Maths: "+ marksInMaths) 
console.log("marks In Science: "+marksInScience) 
console.log("sum of the scores: " + sum)

// any type variable
var xxx: any = 10;
console.log("xxx: " + xxx);

var xxx: any = 10.3;
console.log("xxx: " + xxx);

var xxx: any = "Hello any type";
console.log("xxx: " + xxx);


// Variable Scope

var orgName = "NIIT"          //global variable 

class Employee { 
   employeeId = 13;             //class variable 
   employeeName = "Amit Kumar";
   employeeSalary = 21.1;
   static projectCode = 101;         //static field 
   
   displayEmployeeInfo():void { 
      console.log("Employee Info: ")
	  console.log("Employee Id: " + this.employeeId)  
	  console.log("Employee Name: " + this.employeeName)  
	  console.log("Employee Salary: " + this.employeeSalary)  
   } 
} 
console.log("Global orgName: " + orgName)  
console.log("Static project Code: " + Employee.projectCode)   //static variable  

var obj = new Employee(); 
console.log("employeeId: " + obj.employeeId) 

obj.displayEmployeeInfo();
import { Component } from '@angular/core';

@Component({
  selector: 'my-app',
  template: '<mydir></mydir> <mydir></mydir>'
  //template: '<mydir></mydir>'
  //templateUrl: 'app/app.component.html'
})
export class AppComponent  { 
  title:string = 'Hello.. Thanks for using Angular2'; 
}
